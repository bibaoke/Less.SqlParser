﻿//bibaoke.com

using System;
using System.Collections.Generic;

namespace Less.SqlParser
{
    public class TableCollection
    {
        private List<string> List
        {
            get;
            set;
        }

        internal TableCollection()
        {
            this.List = new List<string>();
        }

        public int Count
        {
            get
            {
                return this.List.Count;
            }
        }

        internal void Add(string table)
        {
            this.List.Add(table);
        }
    }
}
