﻿//bibaoke.com

using System;
using System.Collections.Generic;

namespace Less.SqlParser
{
    public class Select : Statement
    {
        private static HashSet<string> UnavailableColumnNames
        {
            get;
            set;
        }

        public ColumnCollection Columns
        {
            get;
            set;
        }

        public int ColumnsBegin
        {
            get;
            internal set;
        }

        public int ColumnsEnd
        {
            get;
            internal set;
        }

        public From From
        {
            get;
            internal set;
        }

        public Where Where
        {
            get;
            internal set;
        }

        public OrderBy OrderBy
        {
            get;
            internal set;
        }

        public Limit Limit
        {
            get;
            internal set;
        }

        static Select()
        {
            Select.UnavailableColumnNames = new HashSet<string>(new string[]
            {
                "select", "insert", "update", "delete",
                "from", "where", "group", "by", "having", "union",
                "create", "alter", "modify",
                "+", "-", "/", @"\", "%",
            }, StringComparer.OrdinalIgnoreCase);
        }

        public Select(int begin) : base(begin)
        {
            this.Columns = new ColumnCollection();
        }

        internal void AddColumn(Word word)
        {
            if (Select.UnavailableColumnNames.Contains(word.Content))
            {
                throw new ParseException(word);
            }
            else
            {
                if (this.Columns.Count == 0)
                {
                    this.ColumnsBegin = word.Begin;
                }

                this.Columns.Add(word.Content);
            }
        }
    }
}
