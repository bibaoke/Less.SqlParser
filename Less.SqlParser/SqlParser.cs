﻿//bibaoke.com

namespace Less.SqlParser
{
    public static class SqlParser
    {
        public static StatementCollection Parse(string sql)
        {
            ReaderBase reader = new ActionReader();

            Context context = new Context(sql);

            reader.Context = context;

            while (reader.IsNotNull())
            {
                reader = reader.Read();
            }

            return context.Statements;
        }
    }
}
