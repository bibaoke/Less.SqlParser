﻿//bibaoke.com

using System;
using Less.Text;
using System.Collections.Generic;

namespace Less.SqlParser
{
    internal class FromReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            Select select = this.CastToSelect();

            if (word.Separator.IsEmpty())
            {
                this.CurrentStatement.CurrentClause.End = word.WordEnd;

                return this.Pass<EndReader>();
            }

            if (word.Separator.IsWhiteSpace())
            {
                if (word.Content.IsEmpty())
                {
                    return this.Pass<FromReader>();
                }
                else
                {
                    if (select.IsNotNull())
                    {
                        select.From.AddTable(word);
                    }

                    return this.Pass<FromCommaReader>();
                }
            }
            else if (word.Separator == ",")
            {
                if (select.IsNotNull())
                {
                    select.From.AddTable(word);
                }

                return this.Pass<FromReader>();
            }

            return this.Pass<EndReader>();
        }
    }
}
