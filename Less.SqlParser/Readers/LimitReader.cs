﻿//bibaoke.com

using Less.Text;

namespace Less.SqlParser
{
    internal class LimitReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            if (word.Separator == ",")
            {
                return this.Pass<LimitCommaReader>();
            }
            else if (word.Separator.IsWhiteSpace())
            {
                return this.Pass<LimitCommaReader>();
            }
            else if (word.Separator.IsEmpty())
            {
                throw new ParseException(word);
            }
            else
            {
                throw new ParseException(word);
            }
        }
    }
}
