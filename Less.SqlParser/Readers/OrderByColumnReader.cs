﻿//bibaoke.com

using Less.Text;

namespace Less.SqlParser
{
    internal class OrderByColumnReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            if (word.IsNull())
            {
                throw new ParseException(this.LastWord);
            }

            if (word.Separator == ",")
            {
                return this.Pass<OrderByColumnReader>();
            }
            else if (word.Separator.IsWhiteSpace())
            {
                if (word.Content == "")
                {
                    return this.Pass<OrderByColumnReader>();
                }
                else
                {
                    return this.Pass<OrderByColumnCommaReader>();
                }
            }
            else
            {
                throw new ParseException(word);
            }
        }
    }
}
