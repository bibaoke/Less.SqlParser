﻿//bibaoke.com

using System;

namespace Less.SqlParser
{
    internal class EndReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            if (this.CurrentStatement.IsNotNull())
            {
                if (this.CurrentWord.IsNotNull())
                {
                    this.CurrentStatement.End = this.CurrentWord.End;
                }
            }

            return null;
        }
    }
}
