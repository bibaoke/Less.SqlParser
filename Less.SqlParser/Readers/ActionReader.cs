﻿//bibaoke.com

using Less.Text;

namespace Less.SqlParser
{
    internal class ActionReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = null;

            //是否 SQL 的开始
            if (this.CurrentStatement.IsNull())
            {
                word = this.ReadWord();
            }
            else
            {
                //当前 SQL 语句已结束
                if (this.CurrentStatement.End > 0)
                {
                    //读取上一个单词
                    word = this.Context.LastWord;
                }
            }

            if (word.Separator.IsWhiteSpace())
            {
                if (word.Content.CompareIgnoreCase("select"))
                {
                    this.CurrentStatement = new Select(word.Begin);

                    this.Statements.Add(this.CurrentStatement);

                    return this.Pass<SelectColumnReader>();
                }
                else
                {
                    throw new ParseException(word);
                }
            }
            else
            {
                throw new ParseException(word);
            }
        }
    }
}
