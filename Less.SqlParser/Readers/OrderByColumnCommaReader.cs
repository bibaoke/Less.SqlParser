﻿//bibaoke.com

using Less.Text;

namespace Less.SqlParser
{
    internal class OrderByColumnCommaReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            if (word.Separator.IsEmpty())
            {
                this.CurrentStatement.CurrentClause.End = word.End;

                return this.Pass<EndReader>();
            }

            if (word.Separator == ",")
            {
                if (word.Content.IsWhiteSpace())
                {
                    throw new ParseException(word);
                }
                else
                {
                    return this.Pass<OrderByColumnReader>();
                }
            }
            else if (word.Separator.IsWhiteSpace())
            {
                if (word.Content.CompareIgnoreCase("limit"))
                {
                    Select select = this.CastToSelect();

                    select.CurrentClause.End = this.LastWord.WordEnd;

                    select.Limit = new Limit(word.Begin);

                    select.CurrentClause = select.Limit;

                    return this.Pass<LimitReader>();
                }
                else if (word.Content.CompareIgnoreCase("asc"))
                {
                    return this.Pass<OrderByColumnCommaReader>();
                }
                else if (word.Content.CompareIgnoreCase("desc"))
                {
                    return this.Pass<OrderByColumnCommaReader>();
                }
                else
                {
                    throw new ParseException(word);
                }
            }
            else
            {
                throw new ParseException(word);
            }
        }
    }
}
