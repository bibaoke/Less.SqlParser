﻿//bibaoke.com

using Less.Text;

namespace Less.SqlParser
{
    internal class SelectColumnReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            Select select = this.CastToSelect();

            if (word.Separator.IsEmpty())
            {
                if (word.Content.IsWhiteSpace())
                {
                    throw new ParseException(word);
                }
                else
                {
                    if (select.IsNotNull())
                    {
                        select.AddColumn(word);
                    }

                    return this.Pass<EndReader>();
                }
            }
            else if (word.Separator.IsWhiteSpace())
            {
                if (word.Content.IsEmpty())
                {
                    return this.Pass<SelectColumnReader>();
                }
                else
                {
                    if (select.IsNotNull())
                    {
                        select.AddColumn(word);
                    }

                    return this.Pass<SelectColumnCommaReader>();
                }
            }
            else if (word.Separator == ",")
            {
                if (word.Content.IsWhiteSpace())
                {
                    throw new ParseException(word);
                }
                else
                {
                    if (select.IsNotNull())
                    {
                        select.AddColumn(word);
                    }

                    return this.Pass<SelectColumnReader>();
                }
            }
            else
            {
                throw new ParseException(word);
            }
        }
    }
}
