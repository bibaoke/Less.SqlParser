﻿//bibaoke.com

using Less.Text;

namespace Less.SqlParser
{
    internal class LimitCommaReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            if (word.Separator.IsEmpty())
            {
                this.CurrentStatement.CurrentClause.End = this.LastWord.WordEnd;

                return this.Pass<EndReader>();
            }
            else if (word.Separator.IsWhiteSpace())
            {
                word = this.ReadWord();

                if (word.IsNull())
                {
                    this.CurrentStatement.CurrentClause.End = this.LastWord.WordEnd;

                    return this.Pass<EndReader>();
                }

                if (word.Separator.IsEmpty())
                {
                    this.CurrentStatement.CurrentClause.End = word.End;

                    return this.Pass<EndReader>();
                }

                if (ReaderBase.ActionNames.Contains(word.Content))
                {
                    if (this.CurrentStatement is Select)
                    {
                        Select select = (Select)this.CurrentStatement;

                        select.Limit.End = word.WordEnd;

                        select.End = word.WordEnd;

                        return this.Pass<ActionReader>();
                    }
                }
            }
            else
            {
                throw new ParseException(word);
            }

            return this.Pass<EndReader>();
        }
    }
}
