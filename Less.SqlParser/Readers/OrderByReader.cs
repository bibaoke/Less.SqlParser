﻿//bibaoke.com

using System;
using Less.Text;

namespace Less.SqlParser
{
    internal class OrderByReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            if (word.IsNull())
            {
                throw new ParseException(this.LastWord);
            }

            if (!word.Content.CompareIgnoreCase("by"))
            {
                throw new ParseException(word);
            }

            if (word.Separator.IsNotWhiteSpace())
            {
                throw new ParseException(word);
            }

            return this.Pass<OrderByColumnReader>();
        }
    }
}
