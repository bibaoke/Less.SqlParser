﻿//bibaoke.com

using Less.Text;

namespace Less.SqlParser
{
    internal class WhereReader : ReaderBase
    {
        internal override ReaderBase Read()
        {
            Word word = this.ReadWord();

            Select select = this.CastToSelect();

            if (word.Separator.IsEmpty())
            {
                if (select.IsNotNull())
                {
                    select.CurrentClause.End = word.WordEnd;
                }

                return this.Pass<EndReader>();
            }
            else
            {
                if (word.Content.CompareIgnoreCase("order"))
                {
                    if (word.Separator.IsWhiteSpace())
                    {
                        if (select.IsNotNull())
                        {
                            select.CurrentClause.End = this.LastWord.WordEnd;

                            select.OrderBy = new OrderBy(word.Begin);

                            select.CurrentClause = select.OrderBy;

                            return this.Pass<OrderByReader>();
                        }
                        else
                        {
                            throw new ParseException(word);
                        }
                    }
                    else
                    {
                        throw new ParseException(word);
                    }
                }
                else if (word.Content.CompareIgnoreCase("limit"))
                {
                    if (word.Separator.IsWhiteSpace())
                    {
                        select.CurrentClause.End = this.LastWord.WordEnd;

                        select.Limit = new Limit(word.Begin);

                        select.CurrentClause = select.Limit;

                        return this.Pass<LimitReader>();
                    }
                    else
                    {
                        throw new ParseException(word);
                    }
                }
            }

            return this.Pass<WhereReader>();
        }
    }
}
