﻿//bibaoke.com

namespace Less.SqlParser
{
    internal class Context
    {
        internal StatementCollection Statements
        {
            get;
            set;
        }

        internal Statement CurrentStatement
        {
            get;
            set;
        }

        internal int CurrentIndex
        {
            get;
            set;
        }

        internal string Sql
        {
            get;
            set;
        }

        internal Word CurrentWord
        {
            get;
            set;
        }

        internal Word LastWord
        {
            get;
            set;
        }

        internal Context(string sql)
        {
            this.Statements = new StatementCollection();

            this.CurrentIndex = 0;

            this.Sql = sql;
        }
    }
}
