﻿//bibaoke.com

namespace Less.SqlParser
{
    public abstract class Clause
    {
        public int Begin
        {
            get;
            private set;
        }

        public int End
        {
            get;
            internal set;
        }

        internal Clause(int begin)
        {
            this.Begin = begin;
        }
    }
}
