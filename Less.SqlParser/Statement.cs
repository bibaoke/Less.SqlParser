﻿//bibaoke.com

namespace Less.SqlParser
{
    public abstract class Statement
    {
        internal Clause CurrentClause
        {
            get;
            set;
        }

        public int Begin
        {
            get;
            private set;
        }

        public int End
        {
            get;
            internal set;
        }

        public Statement(int begin)
        {
            this.Begin = begin;
        }
    }
}
