﻿//bibaoke.com

using System;
using System.Collections.Generic;

namespace Less.SqlParser
{
    public class From : Clause
    {
        private static HashSet<string> UnavailableTableNames
        {
            get;
            set;
        }

        static From()
        {
            From.UnavailableTableNames = new HashSet<string>(new string[] {
                "select", "insert", "update", "delete",
                "from", "where", "group", "by", "having", "union",
                "create", "alter", "modify",
                "+", "-", "*", "/", @"\", "%",
            }, StringComparer.OrdinalIgnoreCase);
        }

        internal TableCollection Tables
        {
            get;
            private set;
        }

        internal From(int begin) : base(begin)
        {
            this.Tables = new TableCollection();
        }

        internal void AddTable(Word word)
        {
            if (From.UnavailableTableNames.Contains(word.Content))
            {
                throw new ParseException(word);
            }
            else
            {
                this.Tables.Add(word.Content);
            }
        }
    }
}
