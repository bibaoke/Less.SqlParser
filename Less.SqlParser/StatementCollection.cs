﻿//bibaoke.com

using System;
using System.Collections;
using System.Collections.Generic;

namespace Less.SqlParser
{
    public class StatementCollection : IEnumerable<Statement>
    {
        private List<Statement> List
        {
            get;
            set;
        }

        public Statement this[int index]
        {
            get
            {
                return this.List[index];
            }
        }

        public int Count
        {
            get
            {
                return this.List.Count;
            }
        }

        public StatementCollection()
        {
            this.List = new List<Statement>();
        }

        internal void Add(Statement stament)
        {
            this.List.Add(stament);
        }

        public IEnumerator<Statement> GetEnumerator()
        {
            return this.List.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.List.GetEnumerator();
        }
    }
}
