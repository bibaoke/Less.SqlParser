﻿//bibaoke.com

namespace Less.SqlParser
{
    internal class Word
    {
        internal string Content
        {
            get;
            set;
        }

        internal string Separator
        {
            get;
            set;
        }

        internal int Begin
        {
            get;
            set;
        }

        internal int WordEnd
        {
            get;
            set;
        }

        internal int End
        {
            get;
            set;
        }
    }
}
