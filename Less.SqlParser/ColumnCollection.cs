﻿//bibaoke.com

using System.Collections.Generic;

namespace Less.SqlParser
{
    public class ColumnCollection
    {
        private List<string> List
        {
            get;
            set;
        }

        internal ColumnCollection()
        {
            this.List = new List<string>();
        }

        public int Count
        {
            get
            {
                return this.List.Count;
            }
        }

        internal void Add(string column)
        {
            this.List.Add(column);
        }
    }
}
