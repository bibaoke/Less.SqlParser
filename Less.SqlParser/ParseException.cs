﻿//bibaoke.com

using System;
using Less.Text;

namespace Less.SqlParser
{
    public class ParseException : Exception
    {
        internal ParseException(Word word) :
            this(word.End, word.Content.Length > 0 ? word.Content : word.Separator)
        {
            //
        }

        internal ParseException(int index, string near) :
            base("SQL 错误，在位置{0}，“{1}”附近".FormatString(index, near))
        {
            //
        }
    }
}
