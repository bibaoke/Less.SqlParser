﻿using Less.SqlParser;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Less.Text;
using System.Security.Cryptography;
using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            //
            string count = "select * from user where delete = 0 order by id desc limit {0}, {1}".Count();

            Assert.IsTrue(count == "select count(*) from user where delete = 0");

            count = "select * from gsale where del = 0 order by gid desc, sid desc".Count();

            Assert.IsTrue(count == "select count(*) from gsale where del = 0");

            count = "select * from user where delete = 0".Count();

            Assert.IsTrue(count == "select count(*) from user where delete = 0");

            count = "select * from user".Count();

            Assert.IsTrue(count == "select count(*) from user");

            count = "select id, name, dept from user where delete = 0 order by id desc limit {0}, {1}".Count();

            Assert.IsTrue(count == "select count(*) from user where delete = 0");

            count = "select id, name, dept from user where delete = 0".Count();

            Assert.IsTrue(count == "select count(*) from user where delete = 0");

            count = "select id, name, dept from user".Count();

            Assert.IsTrue(count == "select count(*) from user");

            count = "select * from user where exists(select * from dept where dept.id = user.dept and dept.type = 1)".Count();

            Assert.IsTrue(count == "select count(*) from user where exists(select * from dept where dept.id = user.dept and dept.type = 1)");

            count = "select user.* from user, dept where dept.id = user.dept and dept.type = 1".Count();

            Assert.IsTrue(count == "select count(*) from user, dept where dept.id = user.dept and dept.type = 1");

            count = "select user.id , user.name , user.dept from user , dept where dept.id = user.dept".Count();

            Assert.IsTrue(count == "select count(*) from user , dept where dept.id = user.dept");

            count = "select * from glword order by id".Count();

            Assert.IsTrue(count == "select count(*) from glword");

            //
            string and = "select * from user where delete = 0 order by id desc limit {0}, {1}".And("status = 1");

            Assert.IsTrue(and == "select * from user where delete = 0 and status = 1 order by id desc limit {0}, {1}");

            and = "select * from user order by id desc limit {0}, {1}".And("status = 1");

            Assert.IsTrue(and == "select * from user where status = 1 order by id desc limit {0}, {1}");

            and =
                @"select * from user 
                  order by id desc 
                  limit {0}, {1}".And("status = 1");

            Assert.IsTrue(and ==
                @"select * from user where status = 1 
                  order by id desc 
                  limit {0}, {1}");

            and = "SELECT * FROM USER ORDER BY ID DESC LIMIT {0}, {1}".And("status = 1");

            Assert.IsTrue(and.CompareIgnoreCase("select * from user where status = 1 order by id desc limit {0}, {1}"));

            //
            string page = "select * from user where delete = 0 order by id".Page(10, 2, Sql.SqlServer);

            Assert.IsTrue(page == "select * from (select *, row_number() over(order by id) as rn from user where delete = 0) a where rn between 11 and 20");

            page = "select * from user where delete = 0 order by id desc".Page(10, 2, Sql.SqlServer);

            Assert.IsTrue(page == "select * from (select *, row_number() over(order by id desc) as rn from user where delete = 0) a where rn between 11 and 20");
        }
    }
}
